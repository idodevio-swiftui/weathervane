//
//  WeatherService.swift
//  WeatherVane
//
//  Created by Ido Doron on 11/10/20.
//

import Foundation


let API_KEY = "693f734ab575f71d66c229db00d94e62"

class WeatherService {
    
    func getWeather(city:String, completion: @escaping (Weather?) -> ()){
        
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=\(API_KEY)&units=imperial") else {
            completion(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            
            let weatherResponse = try? JSONDecoder().decode(WeatherResponse.self, from: data)
            if let weatherResponse = weatherResponse {
                let weather = weatherResponse.main
                print(weather)
                completion(weather)
            } else {
                completion(nil)
            }
        }.resume()
    }
}
