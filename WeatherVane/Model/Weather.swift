//
//  Weather.swift
//  WeatherVane
//
//  Created by Ido Doron on 11/10/20.
//

import Foundation

struct WeatherResponse: Decodable {
    let main: Weather
}

struct Weather: Decodable {
    var temp: Double?
    var temp_min: Double?
    var temp_max: Double?
}
