//
//  WeatherVaneApp.swift
//  WeatherVane
//
//  Created by Ido Doron on 11/10/20.
//

import SwiftUI

@main
struct WeatherVaneApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
