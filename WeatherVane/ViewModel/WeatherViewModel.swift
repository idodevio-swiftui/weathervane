//
//  WeatherViewModel.swift
//  WeatherVane
//
//  Created by Ido Doron on 11/10/20.
//

import Foundation
import Combine


class WeatherViewModel:ObservableObject {
    
    private var weatherService: WeatherService!
    
    @Published var weather = Weather()
    
    init() {
        self.weatherService = WeatherService()
    }
    
    var temperature: String {
        if let temp = self.weather.temp {
            return String(format: "%.0f", temp)
        } else {
            return ""
        }
    }
    
    var min_temperature: String {
        if let minTemp = self.weather.temp_min {
            return String(format: "%.0f", minTemp)
        } else {
            return ""
        }
    }
    
    var max_temperature: String {
        if let maxTemp = self.weather.temp_max {
            return String(format: "%.0f", maxTemp)
        } else {
            return ""
        }
    }
    
    var cityName = ""
    
    func search() {
        if let city = self.cityName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            fetchWeather(city: city)
        }
    }
    
    private func fetchWeather(city: String) {
        self.weatherService.getWeather(city: city) { weather in
            
            if let weather = weather {
                self.weather = weather
            }

        }
    }
    
}
