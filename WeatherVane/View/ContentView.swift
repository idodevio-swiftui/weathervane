//
//  ContentView.swift
//  WeatherVane
//
//  Created by Ido Doron on 11/10/20.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var weatherVM: WeatherViewModel
    
    init() {
        self.weatherVM = WeatherViewModel()
    }
    
    var body: some View {
        
        VStack(alignment: .center) {
            TextField("Enter City Name", text: self.$weatherVM.cityName) {
                weatherVM.search()
            }
            .padding(.all)
            .multilineTextAlignment(/*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .font(/*@START_MENU_TOKEN@*/.largeTitle/*@END_MENU_TOKEN@*/).foregroundColor(.white)
            Text("Current Temp: \(weatherVM.temperature)").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/).padding(.bottom).foregroundColor(.white)
            HStack {
                Text("Min Temp: \(weatherVM.min_temperature)").font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/).foregroundColor(.white)
                Text("Max Temp: \(weatherVM.max_temperature)").font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/).foregroundColor(.white)
            }
            
        }.frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: 0, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
        .background(Color.blue)
        .edgesIgnoringSafeArea(.all)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
